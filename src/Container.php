<?php 

namespace Grit;

/**
 * Simple injection container.
 * This class is a singleton
 */
class Container extends \ArrayObject
{

	/**
	 * Holds an instance of self
	 * @var object
	 */
	private static $instance;

	/**
	 * Prevents initializing the class with new keyword
	 */
	private function __construct()
	{}

	/**
	 * Sets a new item
	 * @param [type] $key   [description]
	 * @param [type] $class [description]
	 */
	public function set($key,$class)
	{
		if($this->has($key)) {
			throw new \Exception("The key $key already exists in the Container!");
		}
		$this->offsetSet($key,$class);
	}

	/**
	 * Returns the item with a given key
	 * @param  [type] $key [description]
	 * @return [type]      [description]
	 */
	public function get($key)
	{
		return $this->offsetGet($key);
	}

	/**
	 * Checks if a key exists
	 * @param  [type]  $key [description]
	 * @return boolean      [description]
	 */
	public function has($key)
	{
		return offsetExists($key);
	}

	/**
	 * Sets items from an array
	 * @param array $input [description]
	 */
	public function setArray(array $input)
	{
		foreach ($input as $key => $value) {
			$this->set($key,$value);
		}
	}

	/**
	 * Returns an instance of self
	 * @return [type] [description]
	 */
	public static function getInstance()
	{
		if ( !(self::$instance instanceof self) ) {
            self::$instance = new self();
        }
        return self::$instance;
	}
}